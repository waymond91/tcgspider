# Selenium Utilities
from selenium import webdriver
from selenium.webdriver.support.select import Select
from webdriver_manager.chrome import ChromeDriverManager
import time


class TCGSpider:
    def __init__(self,options=[], headless = True ):
        self.page_options_set = False
        self.options = options

        ###### Configure Chrome Broswer ##################
        self.chrome_options = webdriver.ChromeOptions()
        # Disable javascript
        self.chrome_options.add_argument("--disable-javascript")
        self.chrome_options.add_argument("--no-sandbox")

        # Try to disable webrtc
        self.chrome_options.add_argument('--disable-rtc-smoothness-algorithm')
        self.chrome_options.add_argument('--disable-webrtc-hw-decoding')
        self.chrome_options.add_argument('-disable-webrtc-hw-encoding')
        # Start maximized
        self.chrome_options.add_argument('--kiosk')
        # Run headless
        if headless:
            self.chrome_options.add_argument('--headless')
        self.chrome_options.add_argument("--window-size=1920x1080")
        self.tcg_xpaths = {
            'number_of_results': '//*[@id="product-price-table"]/div[1]/div[3]/select',
            'near_mint': '//*[@id="detailsFilters"]/div/div/ul[5]/li[2]/a',
            'lightly_played': '//*[@id="detailsFilters"]/div/div/ul[5]/li[3]/a',
            'non_foil': '//*[@id="detailsFilters"]/div/div/ul[4]/li[2]/a',
            'product_table': '//*[@id="priceTable"]'
        }

        # Clickable filter options on specific card page
        self.card_page_options = {
            'TCGPlayer Direct': '//*[@id="detailsFilters"]/div/div/ul[1]/li[1]/a[1]',
            'Verified Sellers': '//*[@id="detailsFilters"]/div/div/ul[1]/li[2]/a[1]/span',
            'Sellers in Cart': '//*[@id="detailsFilters"]/div/div/ul[1]/li[3]/a[1]/span',
            '4 or more': '//*[@id="detailsFilters"]/div/div/ul[2]/li[2]/a/span',
            'Listings With Photos': '//*[@id="detailsFilters"]/div/div/ul[3]/li[2]/a/span',
            'Listings Without Photos': '//*[@id="detailsFilters"]/div/div/ul[3]/li[3]/a/span',
            'Near Mint': '//*[@id="detailsFilters"]/div/div/ul[5]/li[2]/a',
            'Lightly Played': '//*[@id="detailsFilters"]/div/div/ul[5]/li[3]/a',
            'Moderately Played': '//*[@id="detailsFilters"]/div/div/ul[5]/li[4]/a/span',
            'Heavily Played': '//*[@id="detailsFilters"]/div/div/ul[5]/li[5]/a/span',
            'Damaged': '//*[@id="detailsFilters"]/div/div/ul[5]/li[6]/a/span',
            'Unopened': '//*[@id="detailsFilters"]/div/div/ul[5]/li[7]/a/span',
            'English': '//*[@id="detailsFilters"]/div/div/ul[6]/li[2]/a/span',
            'Chinese (S)': '//*[@id="detailsFilters"]/div/div/ul[6]/li[3]/a/span',
            'All Non-English': '//*[@id="detailsFilters"]/div/div/ul[6]/li[4]/a/span',
            'Chinese (T)': '//*[@id="detailsFilters"]/div/div/ul[6]/li[5]/a/span',
            'French': '//*[@id="detailsFilters"]/div/div/ul[6]/li[6]/a/span',
            'German': '//*[@id="detailsFilters"]/div/div/ul[6]/li[7]/a/span',
            'Italian': '//*[@id="detailsFilters"]/div/div/ul[6]/li[8]/a/span',
            'Japanese': '//*[@id="detailsFilters"]/div/div/ul[6]/li[9]/a/span',
            'Korean': '//*[@id="detailsFilters"]/div/div/ul[6]/li[10]/a/span',
            'Portugese': '//*[@id="detailsFilters"]/div/div/ul[6]/li[11]/a/span',
            'Russian': '//*[@id="detailsFilters"]/div/div/ul[6]/li[12]/a/span',
            'Spanish': '//*[@id="detailsFilters"]/div/div/ul[6]/li[13]/a/span'
        }

        self.card_price_point = 'price-point__data'

        self.tcg_price_names = [
            'Market Price: Normal',
            'Market Price: Foil',
            'Price Trend: Normal',
            'Price Trend: Foil',
            'Buylist Market Price: Normal',
            'Buylist Market Price: Foil',
            'Listed Median: Normal',
            'Listed Median: Foil'
        ]

        self.tcg_seller_classnames = {
            'name': 'seller__name',
            'reputation': 'seller__reputation',
            'condition': 'product-listing__condition',
            'price': 'product-listing__price',
            'shipping': 'product-listing__shipping',
            'qty': 'input-group-append'
        }

        self.init_browser()

    def init_browser(self, wait_time=10):
        self.browser = webdriver.Chrome(ChromeDriverManager().install(), options=self.chrome_options)
        # self.browser.maximize_window()
        self.browser.implicitly_wait(wait_time)

    def get_card_url(self, cardName, setName):
        irregular_punctuation = {
            " " : "-",
            "'" : "",
            "," : "",
            ":" : ""
        }

        url_base = "https://shop.tcgplayer.com/magic/"
        card_url = cardName
        set_url = setName
        for key, value in irregular_punctuation.items():
            card_url = card_url.replace(key, value)
            set_url = set_url.replace(key, value)
        card_url = card_url.lower()
        set_url = set_url.lower()

        url = url_base + set_url + "/" + card_url
        return url

    def set_page_options(self, options):
        self.options = options
        self.page_options_set = False

    # options should be a list of strings that matches the keys card_page_options
    def write_page_options(self, delay_time=2.5):
        if not self.page_options_set:
            for option in self.options:
                option_xpath = self.card_page_options[option]
                # print(option, option_xpath)
                option_element = self.browser.find_element_by_xpath(option_xpath)
                option_element.click()
                time.sleep(delay_time)
            # Set 50 results per page
            # Should find a more robust way to do this:
            Results_Dropdown = self.browser.find_element_by_xpath('//*[@id="product-price-table"]/div[1]/div[3]/select')
            sl = Select(Results_Dropdown)
            sl.select_by_index(2)
            time.sleep(delay_time)
            self.page_options_set = True

    def card_page_scrape(self, cardName, setName):
        url = self.get_card_url(cardName, setName)

        self.browser.get(url)
        self.write_page_options()

        # Scrape the card's price point data
        tcg_prices = {}
        prices = self.browser.find_elements_by_class_name(self.card_price_point)

        j = 0
        for price in prices:
            try:
                key = self.tcg_price_names[j].lower()
                key = key.replace(' ', '_')
                key = key.replace(':', '')
                if 'N/A' in price.text:
                    tcg_prices.update({key: 'N/A'})
                else:
                    price_string = price.text.replace('$', '')
                    tcg_prices.update({key : price_string})
            except(ValueError):
                print('Value Error processing TCG Price Table.')
                print('Key: ', key)
                print('Value: ', price.text)
            j += 1

        # Scrape the card's seller data
        product_table = self.browser.find_elements_by_class_name('product-listing')

        tcg_sellers = []
        for seller in product_table:
            product_listing = {}
            for key, value in self.tcg_seller_classnames.items():
                info = seller.find_element_by_class_name(value).text

                if key != 'price' and key != 'reputation' and key != 'shipping':
                    product_listing[key] = info

                elif key == 'shipping':
                    shipping_string = info
                    if 'Included' in shipping_string:
                        price = 0
                        product_listing[key] = str(price)
                    elif '+ Shipping: $' in shipping_string:
                        shipping_string = shipping_string.replace('+ Shipping: $', "")
                        if 'Free Shipping on Orders Over' not in shipping_string:
                            product_listing[key] = shipping_string

                        else:
                            product_listing[key] = shipping_string[0:3]

                    else:
                        product_listing[key] = info

                elif key == 'price':
                    product_listing[key] = info.replace('$', '')

                elif key == 'qty':
                    product_listing[key] = info.replace('of', '')

                elif key == 'reputation':
                    try:
                        reputation = info[0:info.find('%')]
                        product_listing['reputation'] = reputation
                        total_sales = info[info.find('%') + 1:-1]
                        total_sales = total_sales.replace('(', '')
                        total_sales = total_sales.replace(')', '')
                        total_sales = total_sales.replace(' Sales', '')
                        total_sales = total_sales.replace(',', '')
                        #total_sales = total_sales.replace('+', '')
                        product_listing['total_sales'] = total_sales
                    except (ValueError):
                        print('Could not parse reputation data')
                        print('Reputation info: ', info)

            tcg_sellers.append(product_listing)
        return tcg_prices, tcg_sellers
