# %%
from TCGSpider import TCGSpider
import time
from datetime import datetime
import sqlalchemy
import pandas as pd
from uuid import uuid4
from EBAYSpider import EBAYSpider
from fuzzywuzzy import fuzz
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from mtgsdk import Card
from PIL import Image
import requests
from io import BytesIO

standard_sets = [
    'THB',
    'GRN',
    'ELD',
    'XLN',
    'M20',
    'IKO',
    'RNA',
    'M21',
    'WAR',
]


def plot_card_price(card, sellers, tcg_prices):
    color_map = {'Blue': 'blue',
                 'Black': 'purple',
                 'Green': 'green',
                 'Red': 'red',
                 'White': 'yellow'}

    # Read card image from url
    #image = plt.imread(card.image_url)
    response = requests.get(card.image_url)
    image = Image.open(BytesIO(response.content))

    # Extract prices form data
    prices = []
    shipping = []
    try:
        for seller in sellers:
            shipping.append(seller['price'] + seller['shipping'])
            prices.append(seller['price'])

    except(TypeError, KeyError):
        print('Type error while calculating total cost')
        print('Skipping Data')
        print("Shipping info: ", seller['shipping'])
        pass
        # return -1

    tcg_mid = float(tcg_prices['market_price_normal'])

    # Plot card by color
    if len(card.colors) == 1:
        color = color_map[card.colors[0]]
    else:
        color = 'yellow'

    sns.set_palette(sns.dark_palette(color, reverse=True))

    # Plot Card Image
    try:
        plt.figure()
        plt.subplot(1, 2, 1)
        plt.axis('off')
        plt.imshow(image)
    except (TypeError):
        print('Type Error displaying image')
        return -2

    # Plot Seller Price Distribution
    plt.subplot(1, 2, 2)
    ax = sns.distplot(prices, kde=False, label='Price')
    try:
        sns.distplot(shipping, kde=False, label='Price + Shipping')
    except ValueError:
        pass
    # plt.plot([tcg_mid, tcg_mid], [0, max(prices)], color='k')
    ax.set(xlabel="TCG Price ($)", ylabel='', title=card.name)
    #plt.axvline(tcg_mid, 0, 0.75)
    plt.axvline(tcg_mid , 0, 0.5, color='k')
    plt.show(block=False)
    plt.pause(0.1)
    plt.close()

def scrape_and_insert(crawler, card_id, engine):
    scrape_datetime = datetime.utcnow()
    scrape_id = str(uuid4())
    prices, sellers = crawler.card_page_scrape(card_id['name'], set_name)

    tcg_scrape = {
        'multiverse_id': card_id['multiverse_id'],
        'scrape_id': scrape_id,
        'date': scrape_datetime,
    }
    tcg_scrapes_df = pd.DataFrame(tcg_scrape, index=[0])
    tcg_scrapes_df.to_sql(name='sessions', con=engine, if_exists='append')

    prices.update({'scrape_id': scrape_id})
    scraped_prices_df = pd.DataFrame(prices, index=[0])
    scraped_prices_df.to_sql(name='prices', con=engine, if_exists='append')

    for seller in sellers:
        seller.update({'scrape_id': scrape_id})
        seller['seller_name'] = seller.pop('name')
        scraped_sellers_df = pd.DataFrame(seller, index=[0])
        scraped_sellers_df.to_sql(name='sellers', con=engine, if_exists='append')

    plot_card_price(Card.find(int(card_id['multiverse_id'])), sellers, prices)

# %%
now = datetime.now()
start = datetime(now.year, now.month, now.day-2, now.hour, 0)
finish = datetime(now.year, now.month, now.day-1, now.hour, 0)
crawler = EBAYSpider()
endTimeTo = crawler.getTime(finish)
endTimeFrom = crawler.getTime(start)
q = crawler.CompletedItemsQuery()
q.endTimeFrom = endTimeFrom
q.endTimeTo = endTimeTo
query = q.buildQuery()

sales = crawler.findCompletedItems(query)
# %%
engine = sqlalchemy.create_engine('postgresql://postgres:tarpKing@127.0.0.1:5432/socratica')
sn = pd.read_sql_query('select distinct(set_name)from cards;', con=engine).to_dict()
sn = sn['set_name']
set_names = list(sn.values())

# %%


class MatchedPurchase:
    rank2string = {
        '1st': 'first',
        '2nd': 'second',
        '3rd': 'third',
        '4th': 'fourth',
        '5th': 'fifth',
        '6th': 'sixth',
        '7th': 'seventh',
        '8th': 'eighth',
        '9th': 'ninth',
        '10th': 'tenth'
    }

    def __init__(self, query_result):
        self.engine=engine
        self.set_names = set_names
        self.set_match_percent = 90

        self.complete_query = query_result
        self.ebay_id = query_result['itemId']
        self.ebay_title = query_result['title']
        self.set_matches = []
        self.card_matches = []
        self.multiverse_id = None
        self.qty = '1'
        self.foil = False
        self.language = 'English'

    def match_set(self):
        for set_name in self.set_names:
            ratio = fuzz.partial_ratio(self.ebay_title.lower(), set_name.lower())
            match = {
                'set_name': set_name,
                'match': ratio,
            }
            #print(set_name, ratio)
            if ratio > self.set_match_percent:
                pass

            if ratio >= 100:
                self.set_matches.append(match)
                break

    def match_card(self):
        if self.set_matches:
            for match in self.set_matches:
                set_name = match['set_name']
                cards = pd.read_sql_query(f"select * from cards "
                                          f"where cards.set_name = '{set_name}'",
                                          con=engine).to_dict()
                card_names = list(cards['name'].values())
                for name in card_names:
                    ratio = fuzz.partial_ratio(self.ebay_title.lower(), name.lower())
                    if ratio >= 100:
                        card_match = {
                            'card_name': name,
                            'match': ratio,
                        }
                        self.card_matches.append(card_match)
                        break

    def find_foil(self):
        title = self.ebay_title
        ratio = fuzz.partial_ratio(self.ebay_title.lower(), 'foil')
        if ratio == 100:
            self.foil = True
        else:
            self.foil = False

    def find_language(self):
        languages = ['Chinese', 'French', 'German', 'Italian', 'Japanese', 'Korean', 'Portuguese', 'Russian', 'Spanish']
        for language in languages:
            ratio = fuzz.partial_ratio(self.ebay_title.lower(), language.lower())
            if ratio >= 95:
                self.language = language
                break

    def find_qty(self):
        def enumerate_qtys(max):
            qtys = {}
            nums = np.linspace(1,max,max)
            for qty in nums:
                qty = int(qty)
                qtys.update({
                    str(qty): [f"{qty}x", f"x{qty}"]
                })
            return qtys

        for key, values in enumerate_qtys(20).items():
            for value in values:
                ratio = fuzz.partial_ratio(self.ebay_title.lower(), value)
                if ratio == 100:
                    self.qty = key
                    return

    def __dict__(self):
        classDict = {
            'ebay_id': self.ebay_id,
            'ebay_title': self.ebay_title,
            'set_matches': self.set_matches,
            'name_matches': self.card_matches,
            'multiverse_id': self.multiverse_id,
            'qty': self.qty,
            'foil': self.foil,
            'language': self.language,
        }
        return classDict


matches = []
for sale in sales:
    mp = MatchedPurchase(sale)
    mp.match_set()

    mp.find_qty()
    try:
        print(mp.ebay_title)
        print(f"{mp.set_matches[-1]['set_name']}:{mp.language}:{mp.foil}:{mp.qty}")
        print()
    except IndexError:
        print(f"{mp.language}:{mp.foil}:{mp.qty}")
        print()
    matches.append(mp)
# %%

if __name__ == '__main__':
    start_time = time.time()
    engine = sqlalchemy.create_engine('postgresql://postgres:tarpKing@127.0.0.1:5432/socratica')
    for set_code in standard_sets:
        set_code = set_code.upper()
        cards_df = pd.read_sql_query("select name, multiverse_id from cards where set='"+set_code+"'", con=engine)
        card_ids = cards_df.to_dict('records')
        set_name = pd.read_sql_query("select set_name from sets where code='"+set_code+"'", con=engine)['set_name'].to_list().pop()
        options = ['Near Mint', 'Lightly Played']
        crawler = TCGSpider(options)
        crawler.init_browser()

        for card_id in card_ids:
            print(card_id['name'])
            scrape_and_insert(crawler, card_id, engine)

