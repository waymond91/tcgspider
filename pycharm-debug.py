
sale0 = {'itemId': '224111461072', 'title': "1x Uro, Titan of Nature's Wrath Theros Beyond Death THB MTG NM", 'globalId': 'EBAY-US', 'primaryCategory': {'categoryId': '38292', 'categoryName': 'MTG Individual Cards'}, 'galleryURL': 'https://thumbs1.ebaystatic.com/m/mTbHa5hZhHeHH_vmkQkijNQ/140.jpg', 'viewItemURL': 'https://www.ebay.com/itm/1x-Uro-Titan-Natures-Wrath-Theros-Beyond-Death-THB-MTG-NM-/224111461072', 'paymentMethod': 'PayPal', 'autoPay': 'true', 'postalCode': '283**', 'location': 'Dudley,NC,USA', 'country': 'US', 'shippingInfo': {'shippingType': 'Calculated', 'shipToLocations': 'Worldwide', 'expeditedShipping': 'false', 'oneDayShippingAvailable': 'false', 'handlingTime': '2'}, 'sellingStatus': {'currentPrice': {'_currencyId': 'USD', 'value': '34.99'}, 'convertedCurrentPrice': {'_currencyId': 'USD', 'value': '34.99'}, 'sellingState': 'EndedWithSales'}, 'listingInfo': {'bestOfferEnabled': 'true', 'buyItNowAvailable': 'false', 'startTime': '2020-08-09T11:06:39.000Z', 'endTime': '2020-08-09T17:59:20.000Z', 'listingType': 'FixedPrice', 'gift': 'false'}, 'returnsAccepted': 'false', 'condition': {'conditionId': '1000', 'conditionDisplayName': 'New'}, 'isMultiVariationListing': 'false', 'topRatedListing': 'false'}
sale1 = {'itemId': '124264171306', 'title': 'Cruel Celebrant x2, War of the Spark, NM MTG', 'globalId': 'EBAY-US', 'primaryCategory': {'categoryId': '38292', 'categoryName': 'MTG Individual Cards'}, 'galleryURL': 'https://thumbs3.ebaystatic.com/m/mLIR7NRyESaNj9zY6eBuwMA/140.jpg', 'viewItemURL': 'https://www.ebay.com/itm/Cruel-Celebrant-x2-War-Spark-NM-MTG-/124264171306', 'paymentMethod': 'PayPal', 'autoPay': 'true', 'postalCode': '281**', 'location': 'Shelby,NC,USA', 'country': 'US', 'shippingInfo': {'shippingServiceCost': {'_currencyId': 'USD', 'value': '0.0'}, 'shippingType': 'Free', 'shipToLocations': 'Worldwide', 'expeditedShipping': 'false', 'oneDayShippingAvailable': 'false', 'handlingTime': '1'}, 'sellingStatus': {'currentPrice': {'_currencyId': 'USD', 'value': '2.29'}, 'convertedCurrentPrice': {'_currencyId': 'USD', 'value': '2.29'}, 'sellingState': 'EndedWithSales'}, 'listingInfo': {'bestOfferEnabled': 'true', 'buyItNowAvailable': 'false', 'startTime': '2020-07-17T17:25:06.000Z', 'endTime': '2020-08-09T17:58:52.000Z', 'listingType': 'StoreInventory', 'gift': 'false'}, 'returnsAccepted': 'false', 'condition': {'conditionId': '1000', 'conditionDisplayName': 'New'}, 'isMultiVariationListing': 'false', 'topRatedListing': 'false'}


class MatchedPurchase:

    def __init__(self, query_result):
        self.set_match_percent = 90
        self.complete_query = query_result
        self.ebay_id = query_result['itemId']
        self.ebay_title = query_result['title']
        self.set_matches = []
        self.card_matches = []
        self.multiverse_id = None
        self.qty = '1'
        self.foil = False
        self.language = 'English'



mp1 = MatchedPurchase(sale0)
mp2 = MatchedPurchase(sale1)
print('hmmm')