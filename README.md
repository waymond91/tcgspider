# TCG Spider
This is a small webscraping project to collect sales data of Magic The Gathering cards. Many card shops in the US sell singles of these cards and its a very time intensive process. The card prices fluctuate often due to changes in the game meta.

So before each sale they have to look up each card individually, enter the name the card, its printed edition, and then look at a few different websites to estimate the recent value of the card.

This project looks to use HTML5s new ability to capture video/images straight from peoples smartphone camera so that the cards may be scanned, and their most recent sales data fetched automatically from real sales data - not from 3rd party resellers.

This will allow small store owners to better react to the market.

### Example Scraped Card Prices
![Card Prices GIF](tcg_prices.gif)  
Here are some example scraped cards. The vertical axis is the number of cards for sale
at the price shown on the horizontal axis. The dark bar represents TCG Players "Mid Point Price".

Many of these listing show an extreme value out to the right, that seems like a separate
distrubtion, these are often the price of the foils that I haven't filtered out in this plot.

### Pricing Analysis
The Mid Point Price is commonly (if not ubiquitously) used as a reference point for small card shops across the US
when making pricing choices for face-to-face card sales. Often times the Mid Point Price
is far lower than what card sellers using TCG Players service are actually listing for. 

*This data is used in conjunction with Ebays SDK to capture actual completed sales on the private market
to give small card shop owners a chance to react to changes in the market.* 
 

