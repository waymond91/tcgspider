#! /home/brett/miniconda3/envs/mtgscrape/bin/python
import datetime
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
from mtgsdk import Card


appid_production = 'BrettSmi-MTGScrap-PRD-ee0516b4c-276888cc'


class EBAYSpider:
    def __init__(self):
        self.api = Connection(appid=appid_production, config_file=None)

    # Needs to be datetime.datetime()
    def getTime(self, now):
        # '2018-08-18T23:59:00.000Z'

        time = str(now.year) + '-' + str(now.month).zfill(2) + '-' + str(now.day).zfill(2) + 'T'
        time += str(now.hour).zfill(2) + ':' + str(now.minute).zfill(2) + ':00.000Z'
        return (time)

    def hoursList(self, year, month, day):
        # Date to scrape in a string
        day = str(datetime.date(year, month, day))
        hours_list = list(range(24))
        hours = []
        days = []

        for h in hours_list:
            hours.append(str(h).zfill(2))

        for h in hours:
            days.append(day + 'T' + h + ':00:00.000Z')
        days.append(day + 'T23:59:59.999Z')

        return days

    def printResponseError(self, error):
        print("***************************Query Error*********************************")
        errorMessage = error.dict()

        for key, value in errorMessage['errorMessage']['error'].items():
            print(key, ":", value)
        print("***********************************************************************")

    # This function is basically a wrapper for the ebay api execute('findCompletedItems',items)
    def findCompletedItems(self, query):
        try:
            response = self.api.execute('findCompletedItems', query)
            # print(response.dict())
            try:
                assert (response.reply.ack == 'Success')
            except AssertionError:
                self.printResponseError(response)
            try:
                assert (type(response.reply.timestamp) == datetime.datetime)
                assert (type(response.reply.searchResult.item) == list)

                item = response.reply.searchResult.item[0]
                assert (type(item.listingInfo.endTime) == datetime.datetime)
                assert (type(response.dict()) == dict)

                search_results = response.dict()['searchResult']['item']
                return search_results
            except AttributeError:
                return None
        except ConnectionError as e:
            print(e)
            print(e.response.dict())
            return None

    def scrapeCard(self, cardName, cardSet, qty):

        query = self.CompletedItemsQuery()
        query.keywords = cardName + " " + cardSet + " x" + qty
        print(query.keywords)
        print(query.buildQuery())
        return (self.findCompletedItems(query.buildQuery()))

    def scrapeDay(self, year, month, day, minPrice=1.0, maxPrice=50.0):
        sales = []

        days = self.hoursList(year, month, day)
        minPrice = str(minPrice)
        maxPrice = str(maxPrice)
        item = {'keywords': '',
                'categoryId': '38292',
                'sortOrder': 'EndTimeLatest',
                'paginationInput': {
                    'entriesPerPage': '100',
                    'pageNumber': '1'
                },
                'itemFilter': [
                    {'name': 'EndTimeFrom', 'value': ''},
                    {'name': 'EndTimeTo', 'value': ''},
                    {'name': 'SoldItemsOnly', 'value': True},
                    {'name': 'ListingType', 'value': 'FixedPrice'},
                    {'name': 'MinPrice', 'value': minPrice},
                    {'name': 'MaxPrice', 'value': maxPrice}
                ]}

        for i in range(24):
            # endTimeFrom
            item['itemFilter'][0]['value'] = days[i]
            # endTimeTo
            item['itemFilter'][1]['value'] = days[i + 1]
            # page
            item['paginationInput']['pageNumber'] = 1
            # print(item)
            count = 100
            while (count >= 100):
                # response = api.execute('findCompletedItems', item)
                # results = response.dict()['searchResult']['item']
                results = self.findCompletedItems(item)
                if results:
                    for sale in results:
                        sales.append(sale)
                    # Check how many results we recieved
                    count = len(results)
                    print(results[0]['listingInfo']['endTime'], count, i)
                    # Increment the page number
                    item['paginationInput']['pageNumber'] += 1
                else:
                    count = 0

        return (sales)

    def scrapeSet(self, setAbr):
        # crawler = EBAYSpider()
        cards = Card.where(set=setAbr).all()
        ebay_sales = {}
        card_list = []
        for card in cards:
            sales = self.scrapeCard(card.name, card.set_name, '4')
            if sales:
                card_list.append(card.name)
                ebay_sales[card.name] = sales
                for sale in sales:
                    price = float(sale['sellingStatus']['currentPrice']['value'])
                    if price > 0.20:
                        print(card.name, price, sale['viewItemURL'])

    class CompletedItemsQuery:
        def __init__(self):
            self.keywords = None
            self.pageNumber = None
            self.endTimeFrom = None
            self.endTimeTo = None
            self.minPrice = None
            self.maxPrice = None

            self.queryTemplate = {'keywords': '',
                                  'categoryId': '38292',
                                  'sortOrder': 'EndTimeLatest',
                                  'paginationInput': {
                                      'entriesPerPage': '100',
                                      'pageNumber': '1'
                                  },
                                  'itemFilter': [
                                      # {'name': 'EndTimeFrom', 'value': ''},
                                      # {'name': 'EndTimeTo', 'value': ''},
                                      {'name': 'SoldItemsOnly', 'value': True},
                                      {'name': 'ListingType', 'value': 'FixedPrice'}
                                      # {'name': 'MinPrice', 'value': ''},
                                      # {'name': 'MaxPrice', 'value': ''}
                                  ]}

        def buildQuery(self):
            query = self.queryTemplate
            if self.keywords:
                query['keywords'] = str(self.keywords)

            if self.pageNumber:
                query['paginationInput']['pageNumber'] = str(self.pageNumber)

            if self.endTimeFrom:
                temp = {'name': 'EndTimeFrom', 'value': str(self.endTimeFrom)}
                query['itemFilter'].append(temp)

            if self.endTimeTo:
                temp = {'name': 'EndTimeTo', 'value': str(self.endTimeTo)}
                query['itemFilter'].append(temp)

            if self.minPrice:
                temp = {'name': 'MinPrice', 'value': str(self.minPrice)}
                query['itemFilter'].append(temp)

            if self.maxPrice:
                temp = {'name': 'MaxPrice', 'value': str(self.maxPrice)}
                query['itemFilter'].append(temp)

            return (query)


if __name__ == '__main__':
    import json

    crawler = EBAYSpider()
    sum = 0
    avg = 0
    aug1sales = crawler.scrapeDay(2018, 8, 1)

    with open('aug1sales.json', 'w') as file:
        json.dump(aug1sales, file)
