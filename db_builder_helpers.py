# %%
from mtgsdk import Card, Set
import pandas as pd
import sqlalchemy


def build_cards_by_set(set):
    engine = sqlalchemy.create_engine('postgresql://postgres:tarpKing@127.0.0.1:5432/socratica')
    table_name = set.lower()+'_cards'
    tables = engine.table_names()
    if table_name not in tables:
        card_objects = Card.where(set=set).all()
        cards = [vars(i) for i in card_objects]
        kept_fields = ['name', 'type', 'rarity', 'multiverse_id', 'image_url', 'set', 'set_name', 'id']
        new_cards = []

        for card in cards:
            new_card = {}
            for key, value in card.items():
                if key in kept_fields:
                    new_card.update({key:value})
            new_cards.append(new_card)

        df = pd.DataFrame(new_cards)
        try:
            df.to_sql(name=new_cards[0]['set'].lower() + '_cards', con=engine)
            print('Saved  '+new_cards[0]['set_name']+' to postgres.')
        except IndexError:
            print("Failed to fetch data for "+set)
    else:
        print(table_name+" already exists")


all_sets = Set.all()


def build_cards():
    count = 0
    omitted_sets = ['AMH1', 'F18', 'L12', 'L13', 'L14', 'L15', 'L16', 'L17']

    for set in all_sets:
        if set.code not in omitted_sets:
            build_cards_by_set(set.code)
        else:
            print('Omitted :'+set.name)
        count += 1

# %%

def build_sets():
    sets = [vars(i) for i in all_sets]
    kept_fields = ['code', 'name', 'type', 'release_date', 'block', 'online_only']
    new_sets = []
    for set in sets:
        new_set = {}
        for key, value in set.items():
            if key in kept_fields:
                if key == 'name':
                new_set.update({key: value})
        new_sets.append(new_set)

    df = pd.DataFrame(new_sets)

    engine = sqlalchemy.create_engine('postgresql://postgres:tarpKing@127.0.0.1:5432/socratica')
    df.to_sql(name='sets', con=engine)

# %%

engine = sqlalchemy.create_engine('postgresql://postgres:tarpKing@127.0.0.1:5432/socratica')
tables = engine.table_names()
kept_tables = []
for table in sorted(tables):
    if '_cards' in table:
        kept_tables.append(table)
        #print(table)
    else:
        print(table)



# %%
dfs = []
for table in kept_tables:
    print(table)
    df = pd.read_sql_query("select * from "+'"'+str(table)+'"', con = engine)
    dfs.append(df)

# %%
master_df = pd.concat(dfs)

# %%
master_df.to_sql(name='cards', con=engine)

